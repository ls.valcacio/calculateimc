package com.example.imcmi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var edit_peso: EditText
    private lateinit var edit_altura: EditText
    private lateinit var btn_go: Button
    private lateinit var text_result: TextView
    private lateinit var text_result_desc: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bindings()

        btn_go.setOnClickListener(View.OnClickListener { calculate() })
    }

    private fun calculate(){
        if(!validation()) return

        val peso = edit_peso.text.toString().toFloat()
        val altura = convertMetros(edit_altura.text.toString().toFloat())

        val resultado = peso / (altura * altura)

        printResult(resultado)
    }

    // helpers
    private fun printResult(resultado:Float){
        val message = getString(R.string.result_imc) + " " + resultado.toString()
        text_result.text = message

        when(resultado) {
            in  0.0..18.4 -> text_result_desc.setText(R.string.result_abaixo_peso)
            in  18.5..24.9 -> text_result_desc.setText(R.string.result_peso_normal)
            in  25.0..29.9 -> text_result_desc.setText(R.string.result_peso_sobrepeso)
            in  30.0..34.9 -> text_result_desc.setText(R.string.result_peso_grau1)
            in  35.0..39.9 -> text_result_desc.setText(R.string.result_peso_grau2)
            else -> text_result_desc.setText(R.string.result_peso_grau3)
        }
    }

    private fun convertMetros(value: Float):Float = value / 100

    private fun validation():Boolean{
        if(edit_peso.text.isNullOrEmpty()){
            edit_peso.setError("Digite o peso")
            return false
        }

        if(edit_altura.text.isNullOrEmpty()) {
            edit_altura.setError("Digite o peso")
            return false
        }

        return true
    }

    private fun bindings(){
        edit_peso = findViewById(R.id.edit_peso)
        edit_altura = findViewById(R.id.edit_altura)
        btn_go = findViewById(R.id.btn_go)
        text_result = findViewById(R.id.text_resultado)
        text_result_desc = findViewById(R.id.text_resultado_desc)
    }
}